# rangers_app_log

*For global version, please check `global` branch.*

RangersAppLog的[Flutter插件](https://pub.dev/packages/rangers_applog_flutter_plugin)。支持埋点上报。

Flutter plugin for RangersAppLog.

提示：可以到[Rangers官网](https://datarangers.com.cn/)查看更详细的文档

Note: Refer to more detailed docs at https://datarangers.com/


##### android/build.gradle下添加一下代码,不知道位置参考example

```
maven {
url 'https://artifact.bytedance.com/repository/Volcengine/'
}
```

##### ios/Podfile下添加一下代码,不知道位置参考example
```
# 1. 注意要引入spec
source 'https://cdn.cocoapods.org'
source 'https://github.com/bytedance/cocoapods_sdk_source_repo.git'
source 'https://github.com/volcengine/volcengine-specs.git'

# 2. 参考以下的写法，在ios工程的Podfile中依赖RangersAppLog
pod 'RangersAppLog', '~> 5.6.6', :subspecs => ['Core', 'Log', 'Host/CN']  # 中国区上报
```

### 使用

[example](example/lib/read_id.dart)

```
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:rangers_app_log/rangers_app_log.dart';
import 'package:android_id/android_id.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp();

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _initResult = false;
  final _rangersAppLogPlugin = RangersAppLog();
  AndroidId _androidIdPlugin = AndroidId();

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    // We also handle the message potentially returning null.
    _initResult =
        await _rangersAppLogPlugin.init(appId: '544828', channel: 'test');

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('初始化结果 : $_initResult'),
            ElevatedButton(
                onPressed: () async {
                  final String? androidId = await _androidIdPlugin.getId();
                  _rangersAppLogPlugin.setUserUniqueID(
                      userUniqueId: androidId ?? '');
                },
                child: Text("设置唯一id")),
            ElevatedButton(
                onPressed: () {
                  _rangersAppLogPlugin.onEventRegister(registerWay: 'wechat');
                },
                child: Text("注册事件")),
            ElevatedButton(
                onPressed: () {
                  ///onEventPurchase("gift","flower", "008",1, "wechat","¥", true, 1);/
                  _rangersAppLogPlugin.onEventPurchaseonEventPurchase(
                    commodityType: "gift",
                    tradeName: "flower",
                    productId: "008",
                    productCount: 13,
                    payWay: 'wechat',
                    payCurrency: "¥",
                    paySuccess: true,
                    payMoney: 100,
                  );
                },
                child: Text("购买事件")),
          ],
        ),
      ),
    );
  }
}

```