package com.mhz.rangers_app_log

import android.content.Context
import com.bytedance.ads.convert.BDConvert
import com.bytedance.applog.AppLog
import com.bytedance.applog.InitConfig
import com.bytedance.applog.game.GameReportHelper
import com.bytedance.applog.util.UriConstants
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import org.json.JSONException

import org.json.JSONObject


/** RangersAppLogPlugin */
class RangersAppLogPlugin : FlutterPlugin, MethodCallHandler {
    private lateinit var applicationContext: Context

    /// The MethodChannel that will the communication between Flutter and native Android
    ///
    /// This local reference serves to register the plugin with the Flutter Engine and unregister it
    /// when the Flutter Engine is detached from the Activity
    private lateinit var channel: MethodChannel

    override fun onAttachedToEngine(flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        channel = MethodChannel(flutterPluginBinding.binaryMessenger, "rangers_app_log")
        channel.setMethodCallHandler(this)
        this.applicationContext = flutterPluginBinding.applicationContext
    }

    override fun onMethodCall(call: MethodCall, result: Result) {
        if (call.method == "init") {
            val appId = call.argument<String>("appId")!!
            val channel = call.argument<String>("channel")!!
            val showDebugLog = call.argument<Boolean>("showDebugLog")!!
            val gameModeEnable = call.argument<Boolean>("gameModeEnable")!!
            val config = InitConfig(appId, channel)
            // 设置数据上送地址
            // 设置数据上送地址
            config.setUriConfig(UriConstants.DEFAULT)
            config.isImeiEnable = false //建议关停获取IMEI（出于合规考虑）

            config.isAutoTrackEnabled = true // 全埋点开关，true开启，false关闭

            config.isLogEnable = showDebugLog // true:开启日志，参考4.3节设置logger，false:关闭日志

            AppLog.setEncryptAndCompress(true) // 加密开关，true开启，false关闭
            // 游戏模式，YES会开始 playSession 上报，每隔一分钟上报心跳日志
            config.setEnablePlay(gameModeEnable)
            //SDK会采集OAID、ANDROID_ID和其他的设备特征字段，请遵循相关合规要求在隐私弹窗后采集
            BDConvert.getInstance().init(applicationContext, AppLog.getInstance());
            AppLog.init(applicationContext, config)
            result.success(true)
        } else if (call.method == "setUserUniqueID") {
            val userUniqueID = call.argument<String>("userUniqueId")!!
            AppLog.setUserUniqueID(userUniqueID); // 设置您自己的账号体系ID或设备ID, 并保证其唯一性 ！
            result.success(true)
        } else if (call.method == "onClick") {
            val argument = call.argument<Map<Any, Any>>("params")
            val eventName = call.argument<String>("eventName")!!
            AppLog.onEventV3(eventName, if (argument == null) null else JSONObject(argument))
            result.success(true)
        } else if (call.method == "onEventRegister") {
            val registerWay = call.argument<String>("registerWay")!!
            //内置事件: “注册” ，属性：注册方式，是否成功，属性值为：wechat ，true
            GameReportHelper.onEventRegister(registerWay, true);
            result.success(true)
        } else if (call.method == "onEventPurchase") {
            //内置事件 “支付”，属性：商品类型，商品名称，商品ID，商品数量，支付渠道，币种，是否成功（必传），金额（必传）
            // 付费金额单位为元
            val commodityType = call.argument<String>("commodityType")!!
            val tradeName = call.argument<String>("tradeName")!!
            val productId = call.argument<String>("productId")!!
            val productCount = call.argument<Int>("productCount")!!
            val payWay = call.argument<String>("payWay")!!
            val payCurrency = call.argument<String>("payCurrency")!!
            val paySuccess = call.argument<Boolean>("paySuccess")!!
            val payMoney = call.argument<Int>("payMoney")!!
            GameReportHelper.onEventPurchase(commodityType, tradeName, productId, productCount, payWay, payCurrency, paySuccess, payMoney)
            result.success(true)
        } else {
            result.notImplemented()
        }
    }

    override fun onDetachedFromEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        channel.setMethodCallHandler(null)
    }
}
