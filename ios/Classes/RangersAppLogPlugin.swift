import Flutter
import UIKit
import RangersAppLog

public class RangersAppLogPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "rangers_app_log", binaryMessenger: registrar.messenger())
    let instance = RangersAppLogPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    let arguments = call.arguments as? [String: Any] ?? [String: Any]()
    switch call.method {
    case "init":
        let appId = (arguments["appId"] as! String)
        let channel = (arguments["channel"] as! String)
        let showDebugLog = (arguments["showDebugLog"] as! Bool)
        let gameModeEnable = (arguments["gameModeEnable"] as! Bool)
        let config = BDAutoTrackConfig.init(appID: appId)
        config.channel = channel
//         config.serviceVendor = BDAutoTrackServiceVendor.CN;
        config.autoTrackEnabled = true;
        // 游戏模式，YES会开始 playSession 上报，每隔一分钟上报心跳日志
        config.gameModeEnable = gameModeEnable;
//        config.logNeedEncrypt = [enableEncrypt boolValue];
//        config.abEnable = [enableAB boolValue];
        config.showDebugLog = showDebugLog
        config.logger = { log in
            print("flutter-plugin applog \(log ?? "")")
        }
        config.logNeedEncrypt = true
        BDAutoTrack.start(with: config)
//        config.serviceVendor = BDAutoTrackServiceVendorCN;
        result(true)
    case "setUserUniqueID":
        let userUniqueId = (arguments["userUniqueId"] as! String)
        BDAutoTrack.shared().setCurrentUserUniqueID(userUniqueId)
        result(true)
    case "onClick":
        let eventName = (arguments["eventName"] as! String)
        BDAutoTrack.shared().eventV3(eventName, params: arguments["channel"] as? [AnyHashable : Any])
        result(true)
    case "onEventRegister":
        let registerWay = (arguments["registerWay"] as! String)
        BDAutoTrack.shared().registerEvent(byMethod: registerWay, isSuccess: true)
        result(true)
    case "onEventPurchase":
        let commodityType = (arguments["commodityType"] as! String)
        let tradeName = (arguments["tradeName"] as! String)
        let productId = (arguments["productId"] as! String)
        let productCount = (arguments["productCount"] as! UInt)
        let payWay = (arguments["payWay"] as! String)
        let payCurrency = (arguments["payCurrency"] as! String)
        let paySuccess = (arguments["paySuccess"] as! Bool)
        let payMoney = (arguments["payMoney"] as! UInt64)
        BDAutoTrack.shared().purchaseEvent(withContentType: commodityType, contentName: tradeName, contentID: productId, contentNumber: productCount, paymentChannel: payWay, currency: payCurrency, currency_amount: payMoney, isSuccess: paySuccess)
        result(true)
    default:
      result(FlutterMethodNotImplemented)
    }
  }
}
