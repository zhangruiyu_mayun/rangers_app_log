import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'rangers_app_log_method_channel.dart';

abstract class RangersAppLogPlatform extends PlatformInterface {
  /// Constructs a RangersAppLogPlatform.
  RangersAppLogPlatform() : super(token: _token);

  static final Object _token = Object();

  static RangersAppLogPlatform _instance = MethodChannelRangersAppLog();

  /// The default instance of [RangersAppLogPlatform] to use.
  ///
  /// Defaults to [MethodChannelRangersAppLog].
  static RangersAppLogPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [RangersAppLogPlatform] when
  /// they register themselves.
  static set instance(RangersAppLogPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<bool> init({required String appId, required String channel, required bool showDebugLog, required bool gameModeEnable}) {
    throw UnimplementedError('init() has not been implemented.');
  }

  Future<bool> setUserUniqueID({required String userUniqueId}) {
    throw UnimplementedError('setUserUniqueID() has not been implemented.');
  }

  Future<bool> onClick(
      {required String eventName, Map<dynamic, dynamic>? params}) {
    throw UnimplementedError('onClick() has not been implemented.');
  }

  Future<bool> onEventRegister({required String registerWay}) {
    throw UnimplementedError('onEventRegister() has not been implemented.');
  }

  Future<bool> onEventPurchase(
      {required String commodityType,
      required String tradeName,
      required String productId,
      required int productCount,
      required payWay,
      required String payCurrency,
      required bool paySuccess,
      required int payMoney}) {
    throw UnimplementedError('onEventPurchase() has not been implemented.');
  }
}
