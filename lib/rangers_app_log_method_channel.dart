import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'rangers_app_log_platform_interface.dart';

/// An implementation of [RangersAppLogPlatform] that uses method channels.
class MethodChannelRangersAppLog extends RangersAppLogPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('rangers_app_log');

  @override
  Future<bool> init(
      {required String appId,
      required String channel,
      required bool showDebugLog,
      required bool gameModeEnable}) async {
    final result = await methodChannel.invokeMethod<bool>('init', {
      'appId': appId,
      'channel': channel,
      'showDebugLog': showDebugLog,
      'gameModeEnable': gameModeEnable
    });
    return result!;
  }

  @override
  Future<bool> setUserUniqueID({required String userUniqueId}) async {
    final result = await methodChannel
        .invokeMethod<bool>('setUserUniqueID', {'userUniqueId': userUniqueId});
    return result!;
  }

  @override
  Future<bool> onClick(
      {required String eventName, Map<dynamic, dynamic>? params}) async {
    final result = await methodChannel.invokeMethod<bool>(
        'onClick', {'eventName': eventName, 'params': params});
    return result!;
  }

  @override
  Future<bool> onEventRegister({required String registerWay}) async {
    final result = await methodChannel
        .invokeMethod<bool>('onEventRegister', {'registerWay': registerWay});
    return result!;
  }

  @override
  Future<bool> onEventPurchase(
      {required String commodityType,
      required String tradeName,
      required String productId,
      required int productCount,
      required payWay,
      required String payCurrency,
      required bool paySuccess,
      required int payMoney}) async {
    final result = await methodChannel.invokeMethod<bool>('onEventPurchase', {
      'commodityType': commodityType,
      'tradeName': tradeName,
      'productId': productId,
      'productCount': productCount,
      'payWay': payWay,
      'payCurrency': payCurrency,
      'paySuccess': paySuccess,
      'payMoney': payMoney,
    });
    return result!;
  }
}
