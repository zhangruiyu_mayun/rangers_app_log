import 'rangers_app_log_platform_interface.dart';

class RangersAppLog {
  Future<bool> init({required String appId, required String channel,required bool showDebugLog,required bool gameModeEnable}) {
    return RangersAppLogPlatform.instance.init(appId: appId, channel: channel,showDebugLog: showDebugLog,gameModeEnable:gameModeEnable);
  }

  Future<bool> setUserUniqueID({required String userUniqueId}) {
    return RangersAppLogPlatform.instance
        .setUserUniqueID(userUniqueId: userUniqueId);
  }

  Future<bool> onClick(
      {required String eventName, Map<dynamic, dynamic>? params}) {
    return RangersAppLogPlatform.instance
        .onClick(eventName: eventName, params: params);
  }

  ///onEventRegister("wechat");
  Future<bool> onEventRegister({required String registerWay}) {
    return RangersAppLogPlatform.instance
        .onEventRegister(registerWay: registerWay);
  }

  ///内置事件 “支付”，属性：商品类型，商品名称，商品ID，商品数量，支付渠道，币种，是否成功（必传），金额（必传）
  /// 付费金额单位为元
  ///onEventPurchase("gift","flower", "008",1, "wechat","¥", true, 1);
  Future<bool> onEventPurchaseonEventPurchase({
    required String commodityType,
    required String tradeName,
    required String productId,
    required int productCount,
    required String payWay,
    required String payCurrency,
    required bool paySuccess,
    required int payMoney,
  }) {
    return RangersAppLogPlatform.instance.onEventPurchase(
      commodityType: commodityType,
      tradeName: tradeName,
      productId: productId,
      productCount: productCount,
      payWay: payWay,
      payCurrency: payCurrency,
      paySuccess: paySuccess,
      payMoney: payMoney,
    );
  }
}
