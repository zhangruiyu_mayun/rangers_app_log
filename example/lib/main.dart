import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:rangers_app_log/rangers_app_log.dart';
import 'package:android_id/android_id.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp();

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _initResult = false;
  final _rangersAppLogPlugin = RangersAppLog();
  AndroidId _androidIdPlugin = AndroidId();

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    // We also handle the message potentially returning null.
    _initResult = await _rangersAppLogPlugin.init(
        appId: Platform.isAndroid ? '544828' : '547227',
        channel: 'testhh',
        gameModeEnable: true,
        showDebugLog: true);
  debugPrint('_initResult ${_initResult}');
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Container(
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('初始化结果 : $_initResult'),
              ElevatedButton(
                  onPressed: () async {
                    _rangersAppLogPlugin.setUserUniqueID(
                        userUniqueId: DateTime.now().millisecondsSinceEpoch.toString());
                  },
                  child: Text("设置唯一id")),
              ElevatedButton(
                  onPressed: () {
                    debugPrint('123');
                    _rangersAppLogPlugin.onEventRegister(registerWay: 'wechat');
                  },
                  child: Text("注册事件")),
              ElevatedButton(
                  onPressed: () {
                    ///onEventPurchase("gift","flower", "008",1, "wechat","¥", true, 1);/
                    _rangersAppLogPlugin.onEventPurchaseonEventPurchase(
                      commodityType: "gift",
                      tradeName: "flower",
                      productId: "008",
                      productCount: 13,
                      payWay: 'wechat',
                      payCurrency: "¥",
                      paySuccess: true,
                      payMoney: 100,
                    );
                  },
                  child: Text("购买事件")),
              ElevatedButton(
                  onPressed: () {
                    ///onEventPurchase("gift","flower", "008",1, "wechat","¥", true, 1);/
                    _rangersAppLogPlugin.onClick(eventName: 'click');
                  },
                  child: Text("点击")),
            ],
          ),
        ),
      ),
    );
  }
}
