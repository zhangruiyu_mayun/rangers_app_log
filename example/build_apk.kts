#!/usr/bin/env kscript

package example

import java.util.Scanner
import java.util.UUID
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.io.*


val appName = "rangers_app_log"
println("是否打apk,1是apk,0是aab")
val scan = Scanner(System.`in`)
var buildInfo = if (scan.nextInt() == 1) {
    println("打-apk")
    "apk" to "5.0.2"
} else {
    println("打-aab")
    "aab" to "1.0.2"
}
println("是否正式版,1是正式版,0是测试包")
val isOnline = scan.nextInt() == 1
println("打 ${if (isOnline) "正式版" else "测试包"}")
scan.close()
val buildType = buildInfo.first
val buildName = buildInfo.second
val buildCode = buildName.replace(".", "").toInt()
//println(execCmd(arrayOf("flutter clean")))
//println(execCmd(arrayOf("flutter packages get ")))
val addToCommand =
    "--dart-define=isRelease=true --dart-define=isOnline=${isOnline} --dart-define=appVersion=${buildName}"
val buildApp = mutableListOf<String>("flutter build")
if (buildType == "aab") {
    buildApp.add("appbundle")
} else {
    buildApp.add("apk --split-per-abi")
}
//版本
buildApp.add("--build-name=${buildName}")
buildApp.add("--build-number=${buildCode}")
//正式包属性
buildApp.add(addToCommand)

var commandRun = buildApp.joinToString(" ")
println(commandRun)
println(execCmd(arrayOf(commandRun)))

val day = LocalDateTime.now().format(DateTimeFormatter.ofPattern("MM月dd日_HH时mm分"))
//如果是1那就是64位
println(
    moveApk(
        "${appName}_${buildName}_${if (isOnline) "正式版" else "测试包"}_${day}",
        buildCode,
        buildType
    )
)
println("打包完成")



fun moveApk(appName: String, buildCode: Int, buildType: String): String {
    if (buildType == "apk") {
        File("build/app/outputs/flutter-apk/app-arm64-v8a-release.apk").copyTo(
            File("apk/$buildCode/${appName}_64位.apk"),
            true
        )
        File("build/app/outputs/flutter-apk/app-armeabi-v7a-release.apk").copyTo(
            File("apk/$buildCode/${appName}_32位.apk"),
            true
        )
    } else if (buildType == "aab") {
        File("build/app/outputs/bundle/Release/app-release.aab").copyTo(
            File("apk/$buildCode/${appName}.aab"),
            true
        )
    }
    return "移动完成"
}

/**
 * 执行系统命令, 返回执行结果
 *
 * @param cmd 需要执行的命令
 * @param dir 执行命令的子进程的工作目录, null 表示和当前主进程工作目录相同
 */
fun execCmd(cmd: Array<String>): String? {
    val result = StringBuilder()
    var process: Process? = null
    var bufrIn: BufferedReader? = null
    var bufrError: BufferedReader? = null
    try {
        // 执行命令, 返回一个子进程对象（命令在子进程中执行）
        process = if (cmd.size == 1) Runtime.getRuntime().exec(cmd.first()) else Runtime.getRuntime().exec(cmd)

        // 方法阻塞, 等待命令执行完成（成功会返回0）
        process.waitFor()

        // 获取命令执行结果, 有两个结果: 正常的输出 和 错误的输出（PS: 子进程的输出就是主进程的输入）
        bufrIn = BufferedReader(InputStreamReader(process.inputStream, "UTF-8"))
        bufrError = BufferedReader(InputStreamReader(process.errorStream, "UTF-8"))
        result.append(bufrIn.readLines().joinToString("\n"))
        result.append(bufrError.readLines().joinToString("\n"))
    } finally {
        closeStream(bufrIn)
        closeStream(bufrError)

        // 销毁子进程
        process?.destroy()
    }

    // 返回执行结果
    return result.toString()
}

fun closeStream(stream: Closeable?) {
    if (stream != null) {
        try {
            stream.close()
        } catch (e: Exception) {
            // nothing
        }
    }
}